#include "part1.h"
#include <iostream>
#include <string>
char* string_copy(char* dest, unsigned int destsize, char* src)
{
	int i = 0;
	char* ret = dest;
	*ret = *src;
	while (*++dest = *++src)
		;
	return ret;
}

void part1()
{
	char password[] = "secret";
	char src[] = "hello world!";
	char dest[sizeof(src)];

	string_copy(dest, 11, src);

	std::cout << src << std::endl;
	std::cout << dest << std::endl;
}
